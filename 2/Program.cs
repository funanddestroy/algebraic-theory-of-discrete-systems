﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.White;

            string help = "1 - Свойства алгебраической операции;\n" +
                          "2 - Построение полугруппы по порождающему множеству и определяющим соотношениям;\n" +
                          "3 - Построение полугруппы бинарных отношений по заданному порождающему множеству.\n";

            while (true) {
                Console.WriteLine(help);
                string command = Console.ReadLine();

                switch (command)
                {
                    case "1":
                        string[] lines = System.IO.File.ReadAllLines(@"..\..\in1");

                        string[] stringSet = lines[0].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        Console.WriteLine("\nМножество:");

                        for (int j = 0; j < stringSet.Length; j++)
                        {
                            Console.Write(stringSet[j] + " ");
                        }
                        Console.WriteLine();

                        string[,] table = new string[stringSet.Length, stringSet.Length];

                        Console.WriteLine("\nТаблица операции:");

                        for (int i = 1; i < stringSet.Length + 1; i++)
                        {
                            string[] arr = lines[i].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            for (int j = 0; j < arr.Length; j++)
                            {
                                table[i - 1, j] = arr[j];
                                Console.Write(table[i - 1, j] + " ");
                            }
                            Console.WriteLine();
                        }

                        if (Commutativity(table))
                        {
                            Console.WriteLine("\n - Операция коммутативна\n");
                        }
                        else
                        {
                            Console.WriteLine("\n - Операция некоммутативна\n");
                        }

                        if (Associativity(table, stringSet))
                        {
                            Console.WriteLine(" - Операция ассоциативна\n");
                        }
                        else
                        {
                            Console.WriteLine(" - Операция неассоциативна\n");
                        }

                        break;

                    case "2":

                        lines = System.IO.File.ReadAllLines(@"..\..\in2");

                        stringSet = lines[0].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        Console.WriteLine("\nНачальное множество:");

                        SortedSet<string> set = new SortedSet<string>();
                        for (int j = 0; j < stringSet.Length; j++)
                        {
                            set.Add(stringSet[j]);
                            Console.Write(stringSet[j] + " ");
                        }
                        Console.WriteLine();

                        Dictionary<string, string> rules = new Dictionary<string, string>();

                        Console.WriteLine("\nПравила:");

                        for (int i = 1; i < lines.Length; i++)
                        {
                            string[] arr = lines[i].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            rules.Add(arr[0], arr[1]);
                            Console.WriteLine(arr[0] + " = " + arr[1]);
                        }
                        Console.WriteLine();

                        PrintCayleyTable(set, rules);

                        Console.WriteLine();

                        break;

                    case "3":

                        lines = System.IO.File.ReadAllLines(@"..\..\in3");

                        Console.WriteLine("\nНачальное множество:");

                        HashSet<Matrix> setMatrix = new HashSet<Matrix>();
                        int h = 0;
                        for (int i = 0; i < lines.Length; i++)
                        {
                            int[] arr = lines[i]
                                .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                                .Select(n => int.Parse(n))
                                .ToArray();

                            int[,] matrix = new int[arr.Length, arr.Length];

                            for (int j = 0; j < arr.Length; j++)
                            {
                                matrix[0, j] = arr[j];
                            }
                            i++;
                            for (int k = 1; k < arr.Length; k++)
                            {
                                int[] a = lines[i]
                                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                                    .Select(n => int.Parse(n))
                                    .ToArray();
                                for (int j = 0; j < arr.Length; j++)
                                {
                                    matrix[k, j] = a[j];
                                }
                                i++;
                            }

                            char c = (char)('A' + (h++));
                            setMatrix.Add(new Matrix(c.ToString(), matrix));
                            new Matrix(c.ToString(), matrix).PrintMatrix();
                        }

                        PrintCayleyTableBinaryMatrix(setMatrix);

                        break;

                    case "exit":
                        return;
                }
            }
        }

        static bool Commutativity(string[,] table)
        {
            for (int i = 0; i < table.GetLength(0); i++)
            {
                for (int j = 0; j < table.GetLength(1); j++)
                {
                    if (table[i, j] != table[j, i])
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        static bool Associativity(string[,] table, string[] set)
        {
            Console.WriteLine("Проверка ассоциативности тестом Лайта:\n");
            Dictionary<string, int> pos = new Dictionary<string, int>();
            for (int i = 0; i < set.Length; i++)
            {
                pos.Add(set[i], i);
            }

            foreach (string s in set)
            {
                string[,] lTable = new string[set.Length, set.Length];
                string[,] rTable = new string[set.Length, set.Length];

                for (int i = 0; i < set.Length; i++)
                {
                    for (int j = 0; j < set.Length; j++)
                    {
                        lTable[i, j] = table[pos[table[i, pos[s]]] , j];
                        rTable[i, j] = table[i, pos[table[pos[s], j]]];
                    }
                }
                
                Console.WriteLine("(x" + s + ")z\t\tx(" + s + "z)");
                for (int i = 0; i < set.Length; i++)
                {
                    for (int j = 0; j < set.Length; j++)
                    {
                        if (lTable[i, j] != rTable[i, j]) Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write(lTable[i, j] + " ");
                        if (lTable[i, j] != rTable[i, j]) Console.ForegroundColor = ConsoleColor.White;
                    }
                    Console.Write("\t");
                    for (int j = 0; j < set.Length; j++)
                    {
                        if (lTable[i, j] != rTable[i, j]) Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write(rTable[i, j] + " ");
                        if (lTable[i, j] != rTable[i, j]) Console.ForegroundColor = ConsoleColor.White;
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();

                for (int i = 0; i < set.Length; i++)
                {
                    for (int j = 0; j < set.Length; j++)
                    {
                        if (lTable[i, j] != rTable[i, j])
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        static void PrintCayleyTable(SortedSet<string> set, Dictionary<string, string> rules)
        {
            SortedSet<string> previous = new SortedSet<string>(set);
            SortedSet<string> next = new SortedSet<string>();
            HashSet<string> result = new HashSet<string>(set);

            while (previous.Count != 0)
            {
                next.Clear();
                foreach (string x in previous)
                {
                    foreach (string y in set)
                    {
                        string resOp = ReductionByRule(x + y, rules);
                        if (!result.Contains(resOp))
                        {
                            next.Add(resOp);
                            result.Add(resOp);
                        }

                        resOp = ReductionByRule(y + x, rules);
                        if (!result.Contains(resOp))
                        {
                            next.Add(resOp);
                            result.Add(resOp);
                        }
                    }
                }

                previous = new SortedSet<string>(next);
            }

            string[] resSet = result.ToArray();

            Console.WriteLine("Полученное множество:");
            foreach (string ss in resSet)
            {
                Console.Write(ss + " ");
            }
            Console.WriteLine("\n");

            Console.WriteLine("Таблица Кэли:");
            string str = "";
            string strb = "";

            int m = 0;
            foreach (string s in resSet)
            {
                m = m > s.Length ? m : s.Length;
            }
            m += 2;

            for (int i = 0; i < m; i++) strb += "-";
            strb += "|";

            for (int i = 0; i < m / 2; i++) str += " ";
            str += "*";
            if (m % 2 == 0) for (int i = 0; i < m / 2 - 1; i++) str += " ";
            else for (int i = 0; i < m / 2; i++) str += " ";
            str += "|";
            foreach (string x in resSet)
            {
                if ((m - x.Length) % 2 != 0) str += " ";
                for (int i = 0; i < (m - x.Length) / 2; i++) str += " ";
                str += x;
                for (int i = 0; i < (m - x.Length) / 2; i++) str += " ";
                str += "|";
            }
            Console.WriteLine(str);
            for (int i = 0; i < resSet.Length + 1; i++) Console.Write(strb);
            Console.WriteLine();

            foreach (string x in resSet)
            {
                str = "";
                if ((m - x.Length) % 2 != 0) str += " ";
                for (int i = 0; i < (m - x.Length) / 2; i++) str += " ";
                str += x;
                for (int i = 0; i < (m - x.Length) / 2; i++) str += " ";
                str += "|";

                foreach (string y in resSet)
                {
                    string xy = ReductionByRule(x + y, rules);
                    if ((m - xy.Length) % 2 != 0) str += " ";
                    for (int i = 0; i < (m - xy.Length) / 2; i++) str += " ";
                    str += xy;
                    for (int i = 0; i < (m - xy.Length) / 2; i++) str += " ";
                    str += "|";
                }

                Console.WriteLine(str);
                for (int i = 0; i < resSet.Length + 1; i++) Console.Write(strb);
                Console.WriteLine();
            }
        }

        static string ReductionByRule(string s, Dictionary<string, string> rules)
        {
            foreach (string rule in rules.Keys)
            {
                if (s.Contains(rule))
                {
                    s = s.Replace(rule, rules[rule]);
                    s = ReductionByRule(s, rules);
                }
            }
            return s;
        }

        class Matrix
        {
            public string name;
            public int[,] matrix;

            public Matrix(string name, int[,] matrix)
            {
                this.name = name;
                this.matrix = (int[,])matrix.Clone();
            }

            static public Matrix operator *(Matrix A, Matrix B)
            {
                int[,] C = new int[A.matrix.GetLength(0), B.matrix.GetLength(1)];
                for (int i = 0; i < A.matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < B.matrix.GetLength(1); j++)
                    {
                        C[i, j] = 0;
                        for (int k = 0; k < A.matrix.GetLength(1); k++)
                        {
                            C[i, j] += A.matrix[i, k] * B.matrix[k, j];
                        }
                        if (C[i, j] > 0) C[i, j] = 1;
                    }
                }

                return new Matrix(A.name + B.name, C);
            }

            public bool ContainsIn(HashSet<Matrix> set)
            {
                foreach (Matrix m in set)
                {
                    bool f = false;
                    for (int i = 0; i < matrix.GetLength(0); i++)
                    {
                        for (int j = 0; j < matrix.GetLength(0); j++)
                        {
                            if (matrix[i, j] != m.matrix[i, j])
                            {
                                f = true;
                                break;
                            }
                        }
                        if (f) break;
                    }

                    if (f) continue;
                    return true;
                }
                return false;
            }

            public void PrintMatrix()
            {
                Console.WriteLine(name + ":");
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        Console.Write(matrix[i, j] + " ");
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }

            public Matrix ReductionForm(Matrix[] set)
            {
                foreach (Matrix m in set)
                {
                    bool f = false;
                    for (int i = 0; i < matrix.GetLength(0); i++)
                    {
                        for (int j = 0; j < matrix.GetLength(0); j++)
                        {
                            if (matrix[i, j] != m.matrix[i, j])
                            {
                                f = true;
                                break;
                            }
                        }
                        if (f) break;
                    }

                    if (f) continue;
                    return m;
                }

                return null;
            }

        }

        static void PrintCayleyTableBinaryMatrix(HashSet<Matrix> set)
        {
            HashSet<Matrix> previous = new HashSet<Matrix>(set);
            HashSet<Matrix> next = new HashSet<Matrix>();
            HashSet<Matrix> result = new HashSet<Matrix>(set);

            while (previous.Count != 0)
            {
                next.Clear();
                foreach (Matrix x in previous)
                {
                    foreach (Matrix y in set)
                    {
                        Matrix resOp = x * y;
                        if (!resOp.ContainsIn(result))
                        {
                            next.Add(resOp);
                            result.Add(resOp);
                        }

                        resOp = y * x;
                        if (!resOp.ContainsIn(result))
                        {
                            next.Add(resOp);
                            result.Add(resOp);
                        }
                    }
                }

                previous = new HashSet<Matrix>(next);
            }

            Matrix[] resSet = result.ToArray();

            Console.WriteLine("Полученное множество:");
            foreach (Matrix ss in resSet)
            {
                ss.PrintMatrix();
            }

            Console.WriteLine("Таблица Кэли:");
            string str = "";
            string strb = "";

            int m = 0;
            foreach (Matrix s in resSet)
            {
                m = m > s.name.Length ? m : s.name.Length;
            }
            m += 2;

            for (int i = 0; i < m; i++) strb += "-";
            strb += "|";

            for (int i = 0; i < m / 2; i++) str += " ";
            str += "*";
            if (m % 2 == 0) for (int i = 0; i < m / 2 - 1; i++) str += " ";
            else            for (int i = 0; i < m / 2; i++) str += " ";
            str += "|";
            foreach (Matrix x in resSet)
            {
                if ((m - x.name.Length) % 2 != 0) str += " ";
                for (int i = 0; i < (m - x.name.Length) / 2; i++) str += " ";
                str += x.name;
                for (int i = 0; i < (m - x.name.Length) / 2; i++) str += " ";
                str += "|";
            }
            Console.WriteLine(str);
            for (int i = 0; i < resSet.Length + 1; i++) Console.Write(strb);
            Console.WriteLine();

            foreach (Matrix x in resSet)
            {
                str = "";
                if ((m - x.name.Length) % 2 != 0) str += " ";
                for (int i = 0; i < (m - x.name.Length) / 2; i++) str += " ";
                str += x.name;
                for (int i = 0; i < (m - x.name.Length) / 2; i++) str += " ";
                str += "|";

                foreach (Matrix y in resSet)
                {
                    Matrix xy = (x * y).ReductionForm(resSet);
                    if ((m - xy.name.Length) % 2 != 0) str += " ";
                    for (int i = 0; i < (m - xy.name.Length) / 2; i++) str += " ";
                    str += xy.name;
                    for (int i = 0; i < (m - xy.name.Length) / 2; i++) str += " ";
                    str += "|";
                }

                Console.WriteLine(str);
                for (int i = 0; i < resSet.Length + 1; i++) Console.Write(strb);
                Console.WriteLine();
            }
            Console.WriteLine();
        }

    }
}
