﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Algebraic_theory_of_discrete_systems
{
    class Program
    {
        static int mlen = 0;

        static bool IsReflexivity(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
                if (matrix[i, i] == 0)
                    return false;
            return true;
        }

        static bool IsSymmetry(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                    if (matrix[i, j] != matrix[j, i])
                        return false;
            return true;
        }

        static bool IsAntisymmetry(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                    if ((matrix[i, j] == 1 && matrix[j, i] == 1) && (i != j))
                        return false;
            return true;
        }

        static bool IsTransitivity(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(0); j++)
                    for (int k = 0; k < matrix.GetLength(0); k++)
                        if ((matrix[i, j] == 1) && (matrix[j, k] == 1) && (matrix[i, k] == 0))
                        {
                            return false;
                        }

            return true;
        }

        static bool IsEquivalence(int[,] matrix)
        {
            return IsReflexivity(matrix) && IsSymmetry(matrix) && IsTransitivity(matrix);
        }

        static int[,] ReflexiveClosure(int[,] matrix)
        {
            int[,] ret = new int[matrix.GetLength(0), matrix.GetLength(1)];
            Array.Copy(matrix, ret, matrix.Length);

            for (int i = 0; i < ret.GetLength(0); i++) ret[i, i] = 1;
            return ret;
        }

        static int[,] SymmetricClosure(int[,] matrix)
        {
            int[,] ret = new int[matrix.GetLength(0), matrix.GetLength(1)];
            Array.Copy(matrix, ret, matrix.Length);

            int[,] inversMatrix = new int[matrix.GetLength(0), matrix.GetLength(1)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    inversMatrix[i, j] = matrix[j, i];
                }
            }

            for (int i = 0; i < ret.GetLength(0); i++)
            {
                for (int j = 0; j < ret.GetLength(1); j++)
                {
                    ret[i, j] |= inversMatrix[i, j];
                }
            }
            return ret;
        }

        static int[,] TransitiveClosure(int[,] matrix)
        {
            int[,] ret = new int[matrix.GetLength(0), matrix.GetLength(1)];
            Array.Copy(matrix, ret, matrix.Length);

            int[,] deg = new int[matrix.GetLength(0), matrix.GetLength(1)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    deg[i, j] = matrix[i, j];
                    ret[i, j] = matrix[i, j];
                }
            }

            for (int n = 0; n < matrix.GetLength(0) - 1; n++)
            {
                int[,] pDeg = new int[matrix.GetLength(0), matrix.GetLength(1)];
                for (int i = 0; i < matrix.GetLength(0); i++)
                    for (int j = 0; j < matrix.GetLength(1); j++)
                        pDeg[i, j] = deg[i, j];
                
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        deg[i, j] = 0;
                        for (int k = 0; k < matrix.GetLength(0); k++)
                        {
                            deg[i, j] = (deg[i, j] + pDeg[i, k] * matrix[k, j]) > 0 ? 1 : 0;
                        }
                        ret[i, j] = (ret[i, j] + deg[i, j]) > 0 ? 1 : 0;
                    }
                }
            }
            return ret;
        }

        static int[,] EquivalentClosure(int[,] matrix)
        {
            int[,] R = ReflexiveClosure(matrix);
            int[,] RS = SymmetricClosure(R);
            int[,] RST = TransitiveClosure(RS);
            return RST;
        }

        static int[] RepresentativesSystem(int[,] matrix)
        {
            HashSet<string> set = new HashSet<string>();
            List<int> ret = new List<int>();

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                StringBuilder str = new StringBuilder();
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    str.Append(matrix[i, j]);
                }

                if (!set.Contains(str.ToString()))
                {
                    set.Add(str.ToString());
                    ret.Add(i);
                }
            }

            return ret.ToArray();
        }

        static List<List<int>> BuildHasseDiagram(int[,] matrix, int[] vertex)
        {
            int[,] matrixCopy = new int[matrix.GetLength(0), matrix.GetLength(1)];
            Array.Copy(matrix, matrixCopy, matrix.Length);

            HashSet<int> set = new HashSet<int>();
            HashSet<int> tmpSet = new HashSet<int>();
            List<List<int>> levels = new List<List<int>>();

            for (int i = 0; i < mlen; i++)
            {
                matrixCopy[i, i] = 0;
            }

            for (int k = 0; k < mlen; k++)
            {
                if (set.Contains(k)) continue;
                tmpSet = new HashSet<int>();
                List<int> level = new List<int>();
                for (int j = 0; j < mlen; j++)
                {
                    if (set.Contains(j)) continue;

                    int s = 0;
                    for (int i = 0; i < mlen; i++)
                    {
                        if (set.Contains(i)) continue;
                        s += matrixCopy[i, j];
                    }

                    if (s == 0)
                    {
                        level.Add(j);
                        tmpSet.Add(j);
                    }
                }

                levels.Add(level);
                set.UnionWith(tmpSet); ;
            }
            
            for (int i = 0; i < mlen; i++)
            {
                for (int j = 0; j < mlen; j++)
                {
                    for (int k = 0; k < mlen; k++)
                    {
                        if (matrixCopy[i, j] == 1 && matrixCopy[j, k] == 1 && matrixCopy[i, k] == 1)
                        {
                            matrixCopy[i, k] = 0;
                        }
                    }
                }
            }

            List<int> st = new List<int>();
            for (int i = 0; i < mlen; i++)
            {
                int s = 0;
                for (int j = 0; j < mlen; j++)
                {
                    s += matrixCopy[i, j];
                }
                if (s == 0)
                {
                    st.Add(i);
                }
            }

            List<List<int>> ret = new List<List<int>>();
            ret.Add(levels[0]);
            ret.Add(st);
                        
            (new System.Threading.Thread(delegate () {
                Application.Run(new HasseDiagram(levels, matrixCopy, vertex));
            })).Start();

            return ret;

        }


        class HasseDiagram : Form
        {
            private List<List<int>> levels;
            private int[,] matrix;
            private int[] A;
            private PictureBox pictureBox = new PictureBox();
            private int size = 700;

            public HasseDiagram(List<List<int>> levels, int[,] matrix, int[] A)
            {
                this.levels = levels;
                this.matrix = matrix;
                this.A = A;

                this.Text = "Диаграмма Хассе";

                this.Height = size;
                this.Width = size;

                pictureBox.Dock = DockStyle.Fill;
                pictureBox.BackColor = Color.White;
                pictureBox.Paint += new PaintEventHandler(this.pictureBox_Paint);
                
                this.Controls.Add(pictureBox);
            }

            private void pictureBox_Paint(object sender, PaintEventArgs e)
            {
                Graphics g = e.Graphics;
                SolidBrush solidBrush = new SolidBrush(Color.Black);
                int circleSize = 8;

                int[,] poins = new int[mlen, mlen];

                int h = size / (levels.Count + 1);
                int yPos = size - h;

                for (int i = 0; i < levels.Count; i++)
                {
                    int w = size / (levels[i].Count + 1);
                    int xPos = w;
                    for (int j = 0; j < levels[i].Count; j++)
                    {
                        g.FillEllipse(solidBrush, xPos - circleSize / 2, yPos - circleSize / 2, circleSize, circleSize);
                        poins[levels[i][j], 0] = xPos;
                        poins[levels[i][j], 1] = yPos;
                        g.DrawString(A[levels[i][j]].ToString(), new Font("Arial", 15), Brushes.Blue, xPos, yPos);
                        xPos += w;
                    }
                    yPos -= h;
                }

                for (int i = 0; i < mlen; i++)
                {
                    for (int j = 0; j < mlen; j++)
                    {
                        if (matrix[i, j] == 1)
                        {
                            int x1 = poins[i, 0];
                            int y1 = poins[i, 1];
                            int x2 = poins[j, 0];
                            int y2 = poins[j, 1];
                            
                            Pen pen = new Pen(Color.Black);
                            pen.Width = 2;
                            g.DrawLine(pen, x1, y1, x2, y2);
                        }
                    }
                }
            }
        }

        class Edge
        {
            public int x;
            public int y;

            public Edge(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }


        static void PrintMatrix(int[] matrix, int[] vertex = null)
        {
            Console.WriteLine();

            if (vertex == null)
            {
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    Console.Write(" " + matrix[i]);
                }
            }
            else
            {
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    Console.Write(" " + vertex[matrix[i]]);
                }
            }

            Console.WriteLine("\n");
        }

        static void PrintSQMatrix(int[,] matrix)
        {
            Console.WriteLine();

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(" " + matrix[i, j]);
                }
                Console.WriteLine();
            }

            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            string[] lines = System.IO.File.ReadAllLines(@"..\..\input.txt");
            int[,] matrix = new int[lines.Length - 1, lines.Length - 1];
            mlen = lines.Length - 1;

            int[] vertex = lines[0]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => int.Parse(n))
                    .ToArray();

            for (int i = 1; i < lines.Length; i++)
            {
                int[] arr = lines[i]
                    .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(n => int.Parse(n))
                    .ToArray();
                for (int j = 0; j < arr.Length; j++)
                {
                    matrix[i - 1, j] = arr[j];
                }
            }
            
            Console.WriteLine(" Отношение:");

            PrintSQMatrix(matrix);
            
            Console.WriteLine(" Свойства данного отношения:");

            if (IsReflexivity(matrix))
                Console.WriteLine(" - рефлексивное;");
            else
                Console.WriteLine(" - нерефлексивное;");

            if (IsSymmetry(matrix))
                Console.WriteLine(" - симметричное;");
            else
                Console.WriteLine(" - несимметричное;");

            if (IsAntisymmetry(matrix))
                Console.WriteLine(" - антисимметричное;");
            else
                Console.WriteLine(" - неантисимметричное;");

            if (IsTransitivity(matrix))
                Console.WriteLine(" - транзитивное.");
            else
                Console.WriteLine(" - нетранзитивное.");

            Console.WriteLine();

            Console.WriteLine(" Рефлексивное замыкание: ");
            PrintSQMatrix(ReflexiveClosure(matrix));
            Console.WriteLine(" Симметричное замыкание: ");
            PrintSQMatrix(SymmetricClosure(matrix));
            Console.WriteLine(" Транзитивное замыкание: ");
            PrintSQMatrix(TransitiveClosure(matrix));
            Console.WriteLine(" Эквивалентное замыкание: ");
            PrintSQMatrix(EquivalentClosure(matrix));
            Console.WriteLine(" Система представителей: ");
            PrintMatrix(RepresentativesSystem(matrix), vertex);

            if (!(IsReflexivity(matrix) && IsAntisymmetry(matrix) && IsTransitivity(matrix)))
            {
                Console.WriteLine(" Данное отношение не является отношением порядка\n");
            }
            else
            {
                List<List<int>> minAndMax = BuildHasseDiagram(matrix, vertex);                

                if (minAndMax[0].Count == 1)
                {
                    Console.WriteLine(" Наименьший и минимальный элемент:");
                    PrintMatrix(minAndMax[0].ToArray(), vertex);
                }
                else
                {
                    Console.WriteLine(" Наименьшего элемента нет. Минимальные элементы:");
                    PrintMatrix(minAndMax[0].ToArray(), vertex);
                }                

                if (minAndMax[1].Count == 1)
                {
                    Console.WriteLine(" Наибольший и максмальный элемент:");
                    PrintMatrix(minAndMax[1].ToArray(), vertex);
                }
                else
                {
                    Console.WriteLine(" Наибольшего элемента нет. Максимальные элементы:");
                    PrintMatrix(minAndMax[1].ToArray(), vertex);
                }
            }
        }
    }
}
